# Email obfuscation

## Usage

Include `emailobf.js`.

Create an `<email-obf>` HTML custom element containing an email address (local@sld.tld) such as :

`(ROT13 local)|(reverse SLD)|(TLD)`

You can use [this online tool](https://rot13.com/) to convert the local part with ROT13.

### Example

For `user@example.com`, that would give :

```<email-obf>hfre|elpmaxe|com</email-obf>```
